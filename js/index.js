var user = "";

$(document).ready(function() {

  $("#menu-close").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });

  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });

  // Scrolls to the selected menu item on the page
  $(function() {
    $('a[href*=#]:not([href=#],[data-toggle],[data-target],[data-slide])').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });

  //#to-top button appears after scrolling
  var fixed = false;
  $(document).scroll(function() {
    if ($(this).scrollTop() > 250) {
      if (!fixed) {
        fixed = true;
        $('#to-top').show("slow", function() {
          $('#to-top').css({
            position: 'fixed',
            display: 'block'
          });
        });
      }
    } else {
      if (fixed) {
        fixed = false;
        $('#to-top').hide("slow", function() {
          $('#to-top').css({
            display: 'none'
          });
        });
      }
    }
  });

  $.ajax({
    type: 'GET',
    url: 'http://intranet/sitios/gestiondelconocimiento/_api/web/CurrentUser',
    headers: {
      'accept': 'application/json;odata=verbose'
    },
    contentType: 'application/json;odata=verbose',
  }).done(function(data) {
    var usrId = data.d.LoginName;
    user = usrId.split("urbaser\\")[1];
  }).fail(function() {

  });

  AOS.init({
    duration: 1200,
  })

});

function enviarForm(){
  var inputName = $("#nombreCompleto").val();
  var phone = $("#telefono").val();
  var title = $("#ideaTitulo").val();
  var message = $("#ideaDescripcion").val();

  if(title != "" && message != ""){
    var msgenvio = inputName + " (" + phone + ")\n";
    msgenvio += message;

    $('#result').html("<div class='alert alert-info'><strong> Procesando idea ... </strong></div>");

    $.ajax({
      url: "http://wsgestionideas/wsgestionideas.asmx/insertIdea",
      type: "POST",
      data: "&titulo="+title+"&descripcion="+msgenvio+"&loginAutor="+user+"&campaña=0596224d-7310-41b4-939b-4e5b84690861",
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      dataType: "text",
    }).done(function(data) {
      $('#result').html("<div class='alert alert-success'><strong> Tu idea ha sido procesada </strong></div>");
    }).fail(function() {
      $('#result').html("<div class='alert alert-error'><strong> Ha ocurrido un error </strong></div>");
    });

  }else{
    $('#result').html("<div class='alert alert-warning'><strong> Debes completar los datos de tu idea </strong></div>");
  }

}
